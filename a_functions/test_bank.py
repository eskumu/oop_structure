from pytest import fixture

from .bank import *


@fixture(autouse=True)
def create_sample_bank_accounts():
    """
    This method will be called before each test method.

    See: https://docs.pytest.org/en/latest/fixture.html#fixtures
    """
    create_bank_card("Johnny", 100)
    create_bank_card("Joe", 200)
    create_bank_card("Mary", 231)
    yield


def test__create_bank_card__balance_is_right():
    assert get_balance("Johnny") == 100


def test__deposit_money__balance_increases():
    assert deposit("Johnny", 50)
    assert get_balance("Johnny") == 150


def test__deposit_negative_amount__balance_does_not_change():
    assert not deposit("Joe", -50)
    assert get_balance("Joe") == 200


def test__withdraw_money__balance_decreases():
    assert withdraw("Mary", 31)
    assert get_balance("Mary") == 200


def test__withdraw_negative_amount__balance_does_not_change():
    assert not withdraw("Mary", -3000)
    assert get_balance("Mary") == 231


def test__withdraw_more_money_than_account_has__balance_does_not_change():
    assert not withdraw("Mary", 3000)
    assert get_balance("Mary") == 231


def test__transfer_money__from_account_balance_decreases():
    assert transfer("Johnny", "Mary", 10)
    assert get_balance("Johnny") == 90


def test__transfer_money__to_account_balance_increases():
    assert transfer("Johnny", "Mary", 10)
    assert get_balance("Mary") == 241


def test__transfer_negative_amount__from_account_balance_stays_the_same():
    assert not transfer("Johnny", "Mary", -10)
    assert get_balance("Johnny") == 100


def test__transfer_negative_amount__to_account_balance_stays_the_same():
    assert not transfer("Johnny", "Mary", -10)
    assert get_balance("Mary") == 231


def test__transfer_not_enough_money_on_from_account__from_account_balance_stays_the_same():
    assert not transfer("Johnny", "Mary", 100_000_000)
    assert get_balance("Johnny") == 100


def test__transfer_not_enough_money_on_from_account__to_account_balance_stays_the_same():
    assert not transfer("Johnny", "Mary", 100_000_000)
    assert get_balance("Mary") == 231
