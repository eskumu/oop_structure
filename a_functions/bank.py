"""Bank accounts constructed with functions only"""

from typing import Dict

# Create empty dictionary for accounts.
BANK_CARDS: Dict[str, int] = {}


def create_bank_card(name: str, balance: int) -> None:
    """Create new bank card."""
    BANK_CARDS[name] = balance


def get_balance(name):
    """Get balance of given account."""
    return BANK_CARDS.get(name)


def deposit(name: str, amount: int) -> bool:
    """
    Deposit money to bank account.

    Deposit can be made if amount is not negative.
    """
    if amount < 0:
        return False
    BANK_CARDS[name] += amount
    return True


def withdraw(name: str, amount: int) -> bool:
    """
    Withdraw money from bank account.

    Withdraw can't be made if amount is negative or withdrawal would end with negative balance
    """
    if amount < 0 or BANK_CARDS[name] - amount < 0:
        return False
    BANK_CARDS[name] -= amount
    return True


def transfer(account_from: str, account_to: str, amount: int):
    """
    Transfer money from one account to the other.

    If amount is negative or original account can't withdraw the money. Bank won't transfer money.
    """
    return withdraw(account_from, amount) and deposit(account_to, amount)
