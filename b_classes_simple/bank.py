"""Bank account logic constructed with classes."""


class Card:

    def __init__(self, name: str, balance: int):
        self.name = name
        self._balance = balance

    def get_balance(self):
        return self._balance

    def deposit(self, amount: int) -> bool:
        """
        Deposit money to bank account.

        Deposit can be made if amount is not negative.
        """
        if amount < 0:
            return False
        self._balance += amount
        return True

    def withdraw(self, amount: int) -> bool:
        """
        Withdraw money from bank account.

        Withdraw can be made if amount is not negative and bank balance will stay non negative
        """
        if amount < 0 or self._balance - amount < 0:
            return False
        self._balance -= amount
        return True

    def transfer(self, to_account: "Card", amount: int) -> bool:
        """
        Transfer money from one account to the other.

        If amount is negative or original account can't withdraw the money. Bank won't transfer money.
        """
        return self.withdraw(amount) and to_account.deposit(amount)
