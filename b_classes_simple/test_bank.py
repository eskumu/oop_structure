from pytest import fixture

from .bank import Card


class TestBase:

    @fixture(autouse=True)
    def create_sample_bank_accounts(self):
        """
        This method will be called before each test method.

        See: https://docs.pytest.org/en/latest/fixture.html#fixtures
        """
        self.johnny_card = Card("Johnny", 100)
        self.joe_card = Card("Joe", 200)
        self.mary_card = Card("Mary", 231)
        yield


class TestCreateCard(TestBase):

    def test__create_bank_card__balance_is_right(self):
        assert self.johnny_card.get_balance() == 100


class TestDeposit(TestBase):

    def test__deposit_money__method_returns_true(self):
        assert self.johnny_card.deposit(50)

    def test__deposit_money__balance_increases(self):
        self.johnny_card.deposit(50)
        assert self.johnny_card.get_balance() == 150

    def test__deposit_negative_amount__method_returns_false(self):
        assert not self.johnny_card.deposit(-50)

    def test__deposit_negative_amount__balance_does_not_change(self):
        self.johnny_card.deposit(-50)
        assert self.johnny_card.get_balance() == 100


class TestWithdraw(TestBase):

    def test__withdraw_money__method_returns_true(self):
        assert self.mary_card.withdraw(31)

    def test__withdraw_money__balance_decreases(self):
        self.mary_card.withdraw(31)
        assert self.mary_card.get_balance() == 200

    def test__withdraw_negative_amount__method_returns_false(self):
        assert not self.mary_card.withdraw(-30)

    def test__withdraw_negative_amount__balance_does_not_change(self):
        self.mary_card.withdraw(-30)
        assert self.mary_card.get_balance() == 231

    def test__withdraw_more_money_than_account_has__method_returns_false(self):
        assert not self.mary_card.withdraw(3000)

    def test__withdraw_more_money_than_account_has__balance_does_not_change(self):
        self.mary_card.withdraw(3000)
        assert self.mary_card.get_balance() == 231


class TestTransfer(TestBase):

    def test__transfer_money__method_returns_true(self):
        assert Card.transfer(self.johnny_card, self.mary_card, 10)

    def test__transfer_money__from_account_balance_decreases(self):
        Card.transfer(self.johnny_card, self.mary_card, 10)
        assert self.johnny_card.get_balance() == 90

    def test__transfer_money__to_account_balance_increases(self):
        Card.transfer(self.johnny_card, self.mary_card, 10)
        assert self.mary_card.get_balance() == 241

    def test__transfer_negative_amount__method_returns_false(self):
        assert not Card.transfer(self.johnny_card, self.mary_card, -10)

    def test__transfer_negative_amount__from_account_balance_stays_the_same(self):
        Card.transfer(self.johnny_card, self.mary_card, -10)
        assert self.johnny_card.get_balance() == 100

    def test__transfer_negative_amount__to_account_balance_stays_the_same(self):
        Card.transfer(self.johnny_card, self.mary_card, -10)
        assert self.mary_card.get_balance() == 231

    def test__transfer_not_enough_money_on_from_account__method_returns_false(self):
        assert not Card.transfer(self.johnny_card, self.mary_card, 100_000_000)

    def test__transfer_not_enough_money_on_from_account__from_account_balance_stays_the_same(self):
        Card.transfer(self.johnny_card, self.mary_card, 100_000_000)
        assert self.johnny_card.get_balance() == 100

    def test__transfer_not_enough_money_on_from_account__to_account_balance_stays_the_same(self):
        Card.transfer(self.johnny_card, self.mary_card, 100_000_000)
        assert self.mary_card.get_balance() == 231
