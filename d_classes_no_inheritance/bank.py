"""
Bank account logic constructed with classes.

Using custom exceptions and different card types (types as strings).
"""


class IllegalTransactionException(Exception):
    pass


class Card:
    DEBIT = "DEBIT"
    CREDIT = "CREDIT"

    def __init__(self, name: str, balance: int, card_type="DEBIT", credit_limit=0):
        self.name = name
        self._balance = balance
        self.type = card_type
        self._credit_limit = credit_limit

    def get_balance(self):
        return self._balance

    def deposit(self, amount: int) -> None:
        """
        Deposit money to bank account.

        Deposit can be made if amount is not negative.
        """
        if amount < 0:
            raise IllegalTransactionException("Can't deposit negative amount")
        self._balance += amount

    def withdraw(self, amount: int) -> None:
        """
        Withdraw money from bank account.

        Withdraw can't be made if amount is negative
        or withdrawal would end with negative balance or exceed credit limit if card type is credit.
        """
        if amount < 0:
            raise IllegalTransactionException("Can't withdraw negative amount")
        if self.type == Card.DEBIT and self._balance - amount < 0:
            raise IllegalTransactionException("Can't withdraw more money than account has")
        if self.type == Card.CREDIT and self._balance + self._credit_limit - amount < 0:
            raise IllegalTransactionException("Withdrawal can't exceed credit limit")
        self._balance -= amount

    def transfer(self, to_account: "Card", amount: int) -> None:
        """
        Transfer money from one account to the other.

        If amount is negative or original account can't withdraw the money. Bank won't transfer money.
        """
        self.withdraw(amount)
        to_account.deposit(amount)
