"""
Bank account logic constructed with classes.

Using custom exceptions and different card types (Inheritance example).
"""


class IllegalTransactionException(Exception):
    pass


class DebitCard:
    def __init__(self, name, balance):
        self.name = name
        self._balance = balance

    def get_balance(self) -> int:
        return self._balance

    def deposit(self, amount: int) -> None:
        """
        Deposit money to bank account.

        Deposit can be made if amount is not negative.
        """
        if amount < 0:
            raise IllegalTransactionException("Can't deposit negative amount")
        self._balance += amount

    def withdraw(self, amount: int) -> None:
        """
        Withdraw money from bank account.

        Withdraw can't be made if amount is negative or withdrawal would end with negative balance
        """
        if amount < 0:
            raise IllegalTransactionException("Can't withdraw negative amount")
        if self._balance - amount < 0:
            raise IllegalTransactionException("Can't withdraw more money than account has")
        self._balance -= amount

    def transfer(self, to_account: "DebitCard", amount: int):
        """
        Transfer money from one account to the other.

        If amount is negative or original account can't withdraw the money. Bank won't transfer money.
        """
        self.withdraw(amount)
        to_account.deposit(amount)


class CreditCard(DebitCard):

    def __init__(self, name: str, balance: int, credit_limit=1000):
        super().__init__(name, balance)
        self._credit_limit = credit_limit

    def withdraw(self, amount: int) -> None:
        """
        Withdraw money from bank account.

        Withdraw can't be made if amount is negative or withdrawal would exceed account credit limit
        """
        if amount < 0:
            raise IllegalTransactionException("Can't withdraw negative amount")
        if self._balance + self._credit_limit - amount < 0:
            raise IllegalTransactionException("Withdrawal can't exceed credit limit")
        self._balance -= amount
