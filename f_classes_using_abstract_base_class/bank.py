"""
Bank account logic constructed with classes.

Using custom exceptions and different card types (Inheritance example).
Extending this sample to use abstract base class.
"""

from abc import ABC, abstractmethod


class IllegalTransactionException(Exception):
    pass


class Card(ABC):

    def __init__(self, name: str, balance: int):
        self.name = name
        self._balance = balance

    def get_balance(self) -> int:
        return self._balance

    def deposit(self, amount: int) -> None:
        """
        Deposit money to bank account.

        Deposit can be made if amount is not negative.
        """
        if amount < 0:
            raise IllegalTransactionException("Can't deposit negative amount")
        self._balance += amount

    @abstractmethod  # mark this method as abstract, child classes should implement it.
    def withdraw(self, amount: int):
        """Withdraw money from bank account."""
        pass

    def transfer(self, to_account: "Card", amount: int) -> None:
        """
        Transfer money from one account to the other.

        If amount is negative or original account can't withdraw the money. Bank won't transfer money.
        """
        self.withdraw(amount)
        to_account.deposit(amount)


class DebitCard(Card):
    def withdraw(self, amount: int) -> None:
        if amount < 0:
            raise IllegalTransactionException("Can't withdraw negative amount")
        if self._balance - amount < 0:
            raise IllegalTransactionException("Can't withdraw more money than account has")
        self._balance -= amount


class CreditCard(Card):

    def __init__(self, name: str, balance, credit_limit=1000):
        super().__init__(name, balance)
        self._credit_limit = credit_limit

    def withdraw(self, amount: int) -> None:
        """
        Withdraw money from bank account.

        Withdraw can't be made if amount is negative or withdrawal would exceed account credit limit
        """
        if amount < 0:
            raise IllegalTransactionException("Can't withdraw negative amount")
        if self._balance + self._credit_limit - amount < 0:
            raise IllegalTransactionException("Withdrawal can't exceed credit limit")
        self._balance -= amount
