from pytest import fixture, raises

from .bank import Card, DebitCard, CreditCard, IllegalTransactionException


class TestBase:

    @fixture(autouse=True)
    def create_sample_bank_accounts(self):
        """
        This method will be called before each test method.

        See: https://docs.pytest.org/en/latest/fixture.html#fixtures
        """
        self.johnny_debit_card = DebitCard("Johnny", 100)
        self.johnny_credit_card = CreditCard("Johnny", 1, credit_limit=1000)
        self.joe_card = DebitCard("Joe", 200)
        self.mary_card = DebitCard("Mary", 231)
        yield


class TestCreateCard(TestBase):

    def test__create_debit_card__balance_is_right(self):
        assert self.johnny_debit_card.get_balance() == 100

    def test__create_credit_card__balance_is_right(self):
        assert self.johnny_credit_card.get_balance() == 1


class TestDeposit(TestBase):

    def test__deposit_money__balance_increases(self):
        self.johnny_debit_card.deposit(50)
        assert self.johnny_debit_card.get_balance() == 150

    def test__deposit_negative_amount__method_raises_exception(self):
        with raises(IllegalTransactionException):
            self.johnny_debit_card.deposit(-50)

    def test__deposit_negative_amount__balance_does_not_change(self):
        with raises(IllegalTransactionException):
            self.johnny_debit_card.deposit(-50)
        assert self.johnny_debit_card.get_balance() == 100


class TestWithdraw(TestBase):

    def test__withdraw_debit_card__balance_decreases(self):
        self.mary_card.withdraw(31)
        assert self.mary_card.get_balance() == 200

    def test__withdraw_credit_card__balance_can_be_negative(self):
        self.johnny_credit_card.withdraw(21)
        assert self.johnny_credit_card.get_balance() == -20

    def test__withdraw_debit_card_negative_amount__method_raises_exception(self):
        with raises(IllegalTransactionException):
            self.mary_card.withdraw(-30)

    def test__withdraw_credit_card_negative_amount__method_raises_exception(self):
        with raises(IllegalTransactionException):
            self.johnny_credit_card.withdraw(-30)

    def test__withdraw_debit_card_negative_amount__balance_does_not_change(self):
        with raises(IllegalTransactionException):
            self.mary_card.withdraw(-30)
        assert self.mary_card.get_balance() == 231

    def test__withdraw_credit_card_negative_amount__balance_does_not_change(self):
        with raises(IllegalTransactionException):
            self.johnny_credit_card.withdraw(-30)
        assert self.johnny_credit_card.get_balance() == 1

    def test__withdraw_debit_card_more_money_than_account_has__method_raises_exception(self):
        with raises(IllegalTransactionException):
            self.mary_card.withdraw(3000)

    def test__withdraw_debit_card_more_money_than_account_has__balance_does_not_change(self):
        with raises(IllegalTransactionException):
            self.mary_card.withdraw(3000)
        assert self.mary_card.get_balance() == 231

    def test__withdraw_credit_card_exceeding_credit_limit__method_raises_exception(self):
        with raises(IllegalTransactionException):
            self.johnny_credit_card.withdraw(3000)

    def test__withdraw_credit_card_exceeding_credit_limit__balance_does_not_change(self):
        with raises(IllegalTransactionException):
            self.johnny_credit_card.withdraw(3000)
        assert self.johnny_credit_card.get_balance() == 1


class TestTransfer(TestBase):

    def test__transfer_money__from_account_balance_decreases(self):
        Card.transfer(self.johnny_debit_card, self.mary_card, 10)
        assert self.johnny_debit_card.get_balance() == 90

    def test__transfer_money__to_account_balance_increases(self):
        Card.transfer(self.johnny_debit_card, self.mary_card, 10)
        assert self.mary_card.get_balance() == 241

    def test__transfer_negative_amount__method_raises_exception(self):
        with raises(IllegalTransactionException):
            assert not Card.transfer(self.johnny_debit_card, self.mary_card, -10)

    def test__transfer_negative_amount__from_account_balance_stays_the_same(self):
        with raises(IllegalTransactionException):
            Card.transfer(self.johnny_debit_card, self.mary_card, -10)
        assert self.johnny_debit_card.get_balance() == 100

    def test__transfer_negative_amount__to_account_balance_stays_the_same(self):
        with raises(IllegalTransactionException):
            Card.transfer(self.johnny_debit_card, self.mary_card, -10)
        assert self.mary_card.get_balance() == 231

    def test__transfer_not_enough_money_on_from_account__method_raises_exception(self):
        with raises(IllegalTransactionException):
            Card.transfer(self.johnny_debit_card, self.mary_card, 100_000_000)

    def test__transfer_not_enough_money_on_from_account__from_account_balance_stays_the_same(self):
        with raises(IllegalTransactionException):
            Card.transfer(self.johnny_debit_card, self.mary_card, 100_000_000)
        assert self.johnny_debit_card.get_balance() == 100

    def test__transfer_not_enough_money_on_from_account__to_account_balance_stays_the_same(self):
        with raises(IllegalTransactionException):
            Card.transfer(self.johnny_debit_card, self.mary_card, 100_000_000)
        assert self.mary_card.get_balance() == 231
